from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Add_Status_Form
from .models import Status

# Create your views here.
def status_page(request):
    form = Add_Status_Form(request.POST or None)
    response = {}
    if (request.method == "POST"):
        if (form.is_valid()):
            title = request.POST.get('title')
            description = request.POST.get('description')
            status = Status(title=title, description=description)
            status.save()
    full_status = Status.objects.all()
    response = {
        "allStatus" : full_status
    }
    response['form'] = form
    return render(request,'status_page.html',response)

def landing_page(request):
    return HttpResponseRedirect("/statuspage")
