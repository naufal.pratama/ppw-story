from django import forms
from .models import Status

class Add_Status_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    title_attrs = {
        'type': 'text',
        'class': 'status-form-input',
        'placeholder':'Title of the status'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'status-form-textarea',
        'placeholder':'Description of your status'
    }

    title = forms.CharField(label='', required=True, max_length=300, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True, max_length=300, widget=forms.TextInput(attrs=description_attrs))

