from django.conf.urls import url
from .views import status_page
from .views import landing_page

urlpatterns = [
    url(r'^$', status_page, name='status_page'),
    url(r'^$', landing_page, name='landing_page'),
]
