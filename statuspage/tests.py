from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import status_page
from .views import landing_page
from .models import Status
from .forms import Add_Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class StatusPageTest(TestCase):

    def test_status_page_url_is_exist(self):
        response = Client().get('/statuspage/')
        self.assertEqual(response.status_code,200)

    def test_status_page_using_status_page_template(self):
        response = Client().get('/statuspage/')
        self.assertTemplateUsed(response, 'status_page.html')

    def test_status_page_using_status_page_func(self):
        found = resolve('/statuspage/')
        self.assertEqual(found.func, status_page)

    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,302) #redirect

    def test_landing_page_using_landing_page_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landing_page)
	
    def test_model_can_create_status(self):
        # Creating a new activity
        new_status = Status.objects.create(title='Nggak enak badan', description='Sepertinya kelelahan')
    
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = Add_Status_Form()
        self.assertIn('class="status-form-input', form.as_p())
        self.assertIn('id="id_title"', form.as_p())
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())
    
    def test_form_validation_for_blank_items(self):
        form = Add_Status_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/statuspage/', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 200)
    
        response= Client().get('/statuspage/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/statuspage/', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 200)
    
        response= Client().get('/statuspage/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = './chromedriver.log'
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_can_write_status_and_display_it(self):
        self.selenium.get('http://127.0.0.1:8000')
        self.assertIn('Story PPW', self.selenium.title)
        time.sleep(5)
        title_box = self.selenium.find_element_by_id('id_title')
        desc_box = self.selenium.find_element_by_id('id_description')
        submit_btn = self.selenium.find_element_by_class_name('registerbtn')
        title_box.send_keys('Ini test')
        desc_box.send_keys('Lapar')
        submit_btn.send_keys(Keys.RETURN)
        self.assertIn("Ini test", self.selenium.page_source)
        self.assertIn("Lapar", self.selenium.page_source)
        time.sleep(5)

if __name__ == '__main__': #
    unittest.main(warnings='ignore') #